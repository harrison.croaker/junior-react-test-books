## Junior React Developer Test

### Overview

This test is designed to evaluate your skills in React, API integration, and user interface development. The task is to create a React application that allows users to search for a book by title and displays a list of results with the book's title, first publish year, and first sentence.

### Requirements

1. **User Interface**
   - Create an input field for users to enter a book title.
   - Implement a submit button or trigger to initiate the search.

2. **API Integration**
   - Use the provided Open Library API (https://openlibrary.org/search.json?q=the+lord+of+the+rings) to fetch book data based on the user's search query.

3. **Data Display**
   - Display a list of search results, showing the following information for each book:
     - Title
     - First publish year
     - First sentence (if available)
   - Implement appropriate styling and layout for the list of results.
   - If no results are found, display a message indicating that no books were found for the given search query.

4. **Code Quality**
   - Follow best practices for React component structure and organization.
   - Use appropriate state management techniques.
   - Write clean, readable, and well-documented code.

5. **Bonus Points**
   - Implement loading indicators or placeholders while fetching data.
   - Implement additional features like sorting, filtering, or displaying more book details.

### Submission

To submit your test, please provide the following:

1. A link to a GitHub repository containing your React project.
2. Or an email to harrison.croaker@localsearch.com.au with the zipped code repo.

### Example API Response

Here is an example response from the Open Library API for the search query "the lord of the rings":

```json
{
  "numFound": 1234,
  "start": 0,
  "numFoundExact": true,
  "docs": [
    {
      "key": "/works/OL27258W",
      "title": "The Lord of the Rings",
      "first_publish_year": 1954,
      "first_sentence": "When Mr. Bilbo Baggins of Bag End announced that he would shortly be celebrating his eleventy-first birthday with a party of special magnificence, there was much talk and excitement in Hobbiton."
    },
    {
      "key": "/works/OL27259W",
      "title": "The Lord of the Rings: The Fellowship of the Ring",
      "first_publish_year": 1954,
      "first_sentence": "When Mr. Bilbo Baggins of Bag End announced that he would shortly be celebrating his eleventy-first birthday with a party of special magnificence, there was much talk and excitement in Hobbiton."
    },
    {
      "key": "/works/OL27260W",
      "title": "The Lord of the Rings: The Two Towers",
      "first_publish_year": 1954,
      "first_sentence": "Aragorn sped on up the hill."
    }
  ]
}
```

This example response includes the `numFound` (total number of results), `start` (starting index of the results), `docs` (an array of book objects), and each book object contains the `key`, `title`, `first_publish_year`, and `first_sentence` (if available).

Good luck!

Citations:
[1] https://openlibrary.org/search.json?q=the+lord+of+the+rings